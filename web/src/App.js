import FileViewport from './components/file-viewport';

import './App.css';

function App() {
  const eventMap = {};

  const events = {
    on: (key, cb) => {
      if (!eventMap[key]) {
        eventMap[key] = [];
      }

      eventMap[key].push(cb);
    },
    trigger: (key, e) => {
      eventMap[key]?.forEach((handler) => {
        handler(e);
      });
    },
  };

  // document.addEventListener('click', (x) => events.trigger('click', x));

  return (
    <div className="App">
      <FileViewport events={events} />
    </div>
  );
}

export default App;
