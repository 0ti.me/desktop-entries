const readStringifyFile = async (f) => {
  const fileReader = new FileReader();

  console.error(4, f);
  fileReader.readAsArrayBuffer(f);

  return new Promise((resolve, reject) => {
    fileReader.addEventListener('error', reject);

    fileReader.addEventListener('loadend', () =>
      resolve(new Uint8Array(fileReader.result)),
    );
  });
};

export default readStringifyFile;
