/* eslint-disable eqeqeq */
let textDecoder;
let textEncoder;

const makeKebabCase = (x) => {
  if (!textDecoder) {
    textDecoder = new TextDecoder();
    textEncoder = new TextEncoder();
  }

  const buf = textEncoder.encode(x);

  for (let i = 0; i < buf.length; ++i) {
    if (buf[i] <= 0x5a && buf[i] >= 0x41) {
      buf[i] += 0x20;
    } else if (buf[i] == 0x20 || buf[i] == 0x5f) {
      buf[i] = 0x2d;
    }
  }

  return textDecoder.decode(buf);
};

export default makeKebabCase;
