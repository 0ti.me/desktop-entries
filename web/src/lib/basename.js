// a naive implementation of path.basename for the browser
const basename = (f) => /([^/]+)$/.exec(f)?.[1] ?? '';

export default basename;
