import Input from '@mui/material/Input';

import readStringifyFile from '../lib/read-stringify-file';

const FileInput = ({ onRead, ...props }) => {
  return (
    <Input
      {...props}
      onChange={(e) =>
        (async () => {
          const file = e.target.files[0];

          onRead({
            contents: await readStringifyFile(file),
            filename: file.name,
          });
        })()
      }
      type="file"
    />
  );
};

export default FileInput;
