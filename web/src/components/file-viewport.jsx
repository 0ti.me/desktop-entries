import React, { useEffect, useState } from 'react';
import Badge from '@mui/material/Badge';
import ComputerIcon from '@mui/icons-material/Computer';
import Container from '@mui/material/Container';
import FormGroup from '@mui/material/FormGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import NewDesktopEntry from './new-desktop-entry';
import Switch from '@mui/material/Switch';

const FileViewport = () => {
  const [fileInfo, setFileInfo] = useState({ local: [], global: [] });
  const [refreshFiles, setRefreshFiles] = useState(false);
  const [viewGlobal, setViewGlobal] = useState(false);

  let busy = false;

  useEffect(() => {
    if (busy) return;

    // eslint-disable-next-line react-hooks/exhaustive-deps
    busy = true;

    (async () => {
      const val = await fetch('/api/desktop-entries');
      let data;

      if (val.headers.get('content-type').indexOf('json') >= 0) {
        data = await val.json();
      } else {
        data = await val.text();
      }

      setFileInfo(data.data);

      busy = false;
    })();
  }, [refreshFiles]);

  let files;

  if (viewGlobal) {
    files = fileInfo.local.concat(fileInfo.global);
  } else {
    files = fileInfo.local;
  }

  const fileComponents = files.map((ea, i) => (
    <div key={`file-div-${i}`}>{ea}</div>
  ));

  return (
    <>
      <Container>
        <FormGroup>
          <FormControlLabel
            control={
              <Switch onChange={(e) => setViewGlobal(e.target.checked)} />
            }
            label="Show global files"
          />
        </FormGroup>
        <NewDesktopEntry onSave={() => setRefreshFiles(!refreshFiles)} />
        <Badge badgeContent={files.length} color="primary" showZero>
          <ComputerIcon color="action" />
        </Badge>
        {fileComponents}
      </Container>
    </>
  );
};

export default FileViewport;
