import React, { forwardRef, useState } from 'react';
import Accordion from '@mui/material/Accordion';
import AccordionDetails from '@mui/material/AccordionDetails';
import AccordionSummary from '@mui/material/AccordionSummary';
import AppBar from '@mui/material/AppBar';
import Button from '@mui/material/Button';
import CloseIcon from '@mui/icons-material/Close';
import Dialog from '@mui/material/Dialog';
import Divider from '@mui/material/Divider';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import IconButton from '@mui/material/IconButton';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import pako from 'pako';
import Slide from '@mui/material/Slide';
import TextField from '@mui/material/TextField';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';

import FileInput from './file-input';
import IconBrowser from './icon-browser';
import makeKebabCase from '../../src/lib/make-kebab-case';
import readStringifyFile from '../lib/read-stringify-file';

const Transition = forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

export default function FullScreenDialog(props) {
  const [command, setCommand] = useState('');
  const [directory, setDirectory] = useState('');
  const [iconFile, setIconFile] = useState('');
  const [iconFilename, setIconFilename] = useState('');
  const [kebabCaseName, setKebabCaseName] = useState('');
  const [keywords, setKeywords] = useState('');
  const [name, setName] = useState('');
  const [open, setOpen] = useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);

    setKebabCaseName('');
  };

  const handleSave = async () => {
    const iconData = iconFile
      ? window.btoa(String.fromCharCode.apply(null, iconFile))
      : null;

    await fetch('/api/desktop-entries', {
      body: pako.deflate(
        JSON.stringify({
          command,
          directory,
          filename: `${kebabCaseName}.desktop`,
          icon: iconFile
            ? {
              data: iconData,
              encoding: 'base64',
              filename: iconFilename,
              type: 'Uint8Array',
            }
            : { filename: iconFilename, type: 'path' },
          keywords,
          name,
        }),
      ),
      headers: {
        'content-encoding': 'deflate',
        'content-type': 'application/json; charset=utf-8',
      },
      method: 'POST',
    });

    handleClose();

    props.onSave();
  };

  const polishKeywords = (val) => val.split(/, /).join(',');

  const onDragOver = (e) => {
    e.stopPropagation();
    e.preventDefault();

    e.dataTransfer.dropEffect = 'copy';
  };

  const onDrop = async (e) => {
    e.stopPropagation();
    e.preventDefault();

    const files = e.dataTransfer.files;

    for (const f of files) {
      if (f.name.endsWith('.png')) {
        const x = await readStringifyFile(f);

        setIconFile(x);
        setIconFilename(f.name);
      }
    }
  };

  return (
    <div onDragOver={onDragOver} onDrop={onDrop}>
      <Button variant="outlined" onClick={handleClickOpen}>
        New Desktop Entry
      </Button>
      <Dialog
        fullScreen
        open={open}
        onClose={handleClose}
        TransitionComponent={Transition}
      >
        <AppBar sx={{ position: 'relative' }}>
          <Toolbar>
            <IconButton
              edge="start"
              color="inherit"
              onClick={handleClose}
              aria-label="close"
            >
              <CloseIcon />
            </IconButton>
            <Typography sx={{ ml: 2, flex: 1 }} variant="h6" component="div">
              New Desktop Entry
            </Typography>
            <Button autoFocus color="inherit" onClick={handleSave}>
              save
            </Button>
          </Toolbar>
        </AppBar>
        <List>
          <ListItem>
            <TextField
              id="name"
              label="Name"
              onChange={(e) => {
                setName(e.target.value);
                setKebabCaseName(makeKebabCase(e.target.value));
              }}
              required
              variant="standard"
            />
          </ListItem>
          <ListItem>
            <TextField
              defaultValue=""
              id="kebab-case-name"
              InputProps={{
                readOnly: true,
                value: kebabCaseName,
              }}
              label="kebab-case-name"
              variant="standard"
            />
          </ListItem>
          <ListItem>
            <TextField
              id="icon-name"
              InputProps={{
                readOnly: true,
                value: iconFilename,
              }}
              label="Icon Name"
              variant="standard"
            />
          </ListItem>
          <FileInput
            onRead={(x) => {
              setIconFile(x.contents);
              setIconFilename(x.filename);
            }}
          />
          <Accordion TransitionProps={{ unmountOnExit: true }}>
            <AccordionSummary expandIcon={<ExpandMoreIcon />}>
              <Typography>Choose an icon</Typography>
            </AccordionSummary>
            <AccordionDetails>
              <IconBrowser
                onSelect={(e) => {
                  setIconFilename(e);
                }}
              />
            </AccordionDetails>
          </Accordion>
          <ListItem>
            <TextField
              id="directory"
              label="Directory"
              onChange={(e) => setDirectory(e.target.value)}
              variant="standard"
            />
          </ListItem>
          <ListItem>
            <TextField
              id="command"
              label="Command"
              onChange={(e) => setCommand(e.target.value)}
              required
              variant="standard"
            />
          </ListItem>
          <ListItem>
            <TextField
              id="keywords"
              label="Keywords"
              onChange={(e) => setKeywords(polishKeywords(e.target.value))}
              required
              variant="standard"
            />
          </ListItem>
          <Divider />
        </List>
      </Dialog>
    </div>
  );
}
