import React, { useEffect, useState } from 'react';
import IconButton from '@mui/material/IconButton';
import ImageList from '@mui/material/ImageList';
import ImageListItem from '@mui/material/ImageListItem';
import ImageListItemBar from '@mui/material/ImageListItemBar';
import InfoIcon from '@mui/icons-material/Info';
import ListSubheader from '@mui/material/ListSubheader';

import basename from '../lib/basename';

const IconBrowser = (props) => {
  const [itemData, setItemData] = useState([]);

  let busy = false;

  useEffect(() => {
    if (busy) return;

    // eslint-disable-next-line react-hooks/exhaustive-deps
    busy = true;

    (async () => {
      const val = await fetch('/usr/share/icons/mate/256x256/apps');
      let data;

      if (val.headers.get('content-type').indexOf('json') >= 0) {
        data = await val.json();
      } else {
        data = await val.text();
      }

      setItemData(
        data.data.map((filepath) => ({
          img: filepath,
          title: basename(filepath),
        })),
      );

      busy = false;
    })();
  }, [setItemData]);
  /*
    * {
    img: 'https://images.unsplash.com/photo-1551963831-b3b1ca40c98e',
    title: 'Breakfast',
    author: '@bkristastucchio',
    rows: 2,
    cols: 2,
    featured: true,
  }
*/

  return (
    <ImageList sx={{ width: '100%', height: 450 }}>
      <ImageListItem key="Subheader" cols={5}>
        <ListSubheader component="div">December</ListSubheader>
      </ImageListItem>
      {itemData.map((item) => (
        <ImageListItem key={item.img}>
          <IconButton
            edge="start"
            color="inherit"
            onClick={(e) => props.onSelect(item.img, e)}
            aria-label="select"
          >
            <img
              src={`${item.img}?w=248&fit=crop&auto=format`}
              srcSet={`${item.img}?w=248&fit=crop&auto=format&dpr=2 2x`}
              alt={item.title}
              loading="lazy"
            />
          </IconButton>
          <ImageListItemBar
            title={item.title}
            subtitle={item.author}
            actionIcon={
              <IconButton
                sx={{ color: 'rgba(255, 255, 255, 0.54)' }}
                aria-label={`info about ${item.title}`}
              >
                <InfoIcon />
              </IconButton>
            }
          />
        </ImageListItem>
      ))}
    </ImageList>
  );
};

export default IconBrowser;
