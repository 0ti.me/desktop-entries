const { set } = require('@0ti.me/tiny-pfp');
const base = require('@0ti.me/test-deps/configuration-templates/eslintrc.js');

const publicRule = {
  env: {
    browser: true,
    jquery: true,
  },
  files: [
    './client/*.js',
    './client/**/*.js',
    './public/*.js',
    './public/**/*.js',
    './src/client/*.js',
    './src/client/**/*.js',
  ],
};

if (base.overrides) {
  base.overrides.push(publicRule);
} else {
  base.overrides = [publicRule];
}

base.overrides.push({
  files: ['./config/local.js'],
  rules: { 'no-useless-escape': 'off' },
});

const ignorePatterns = [
  '*-ignore',
  'mongo-data-db',
  'mlbud-pg-data',
  '**/*.min.js',
  'dist/*.js',
  'dist/**/*.js',
];

if (base.ignorePatterns) {
  for (const pattern of ignorePatterns) {
    base.ignorePatterns.push(pattern);
  }
} else {
  base.ignorePatterns = ignorePatterns;
}

base.env.es2020 = true;
const prettier = base.extends.pop();

if (prettier !== 'prettier') {
  throw new Error('oops');
}

base.extends.push('plugin:react/recommended');
base.extends.push('plugin:react-hooks/recommended');
base.extends.push(prettier);

base.parser = 'babel-eslint';

set(base, 'parserOptions.sourceType', 'module');
set(base, 'parserOptions.ecmaFeatures.jsx', true);
set(base, 'rules.no-extra-boolean-cast', 'off');
set(base, 'rules.react/prop-types', 'off');
set(base, 'settings.react.version', 'detect');

module.exports = base;

if (require.main === module) {
  /* eslint-disable-next-line no-console */
  console.error(module.exports);
}
