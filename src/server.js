import config from 'config';

import app from './app.js';

const port = config?.server?.port ?? 3001;

export default app.listen(port, () => {
  console.error('up on', port);
});
