import * as fs from 'fs/promises';
import * as path from 'path';

import icons from './icons.js';
import signalProc from './signal-proc.js';

const homeLocalShareApplicationsDir = path.resolve(
  process.env.HOME,
  '.local/share/applications',
);
const usrShareApplicationsDir = '/usr/share/applications';

const relevantDirectories = [
  usrShareApplicationsDir,
  homeLocalShareApplicationsDir,
];

const desktopEntries = {
  dirs: {
    homeLocalShareApplicationsDir,
    usrShareApplicationsDir,
  },
  list: async () => {
    const [rootUsrShareApplications, homeLocalShareApplications] =
      await Promise.all(
        relevantDirectories.map((dir) =>
          fs.readdir(dir).then((l) => l.map((f) => path.resolve(dir, f))),
        ),
      );

    return {
      local: homeLocalShareApplications,
      global: rootUsrShareApplications,
    };
  },
  save: async (body) => {
    const filename = path.resolve(
      desktopEntries.dirs.homeLocalShareApplicationsDir,
      body.filename,
    );

    let decoded, typed;
    let icon = body.icon;

    const hundy = body.icon.data.substring(0, 100);

    switch (body.icon?.encoding) {
      case 'base64':
        decoded = Buffer.from(body.icon.data, 'base64');
        break;
      case '':
      case undefined:
      case null:
      default:
        decoded = body.icon.data;
    }

    switch (body.icon?.type) {
      case 'Uint8Array':
        typed = new Uint8Array(decoded);
        break;
      default:
        typed = decoded;
    }

    if (typed instanceof Uint8Array) {
      // save
      icon = await icons.write(body.icon.filename, typed);
    }

    const command = body.directory
      ? `bash -c 'cd ${body.directory} && ${body.command}'`
      : body.command;

    const toWrite = [
      '[Desktop Entry]',
      'Version=1.0',
      `Name=${body.name}`,
      'Type=Application',
      `Exec=${command}`,
      body.keywords ? `Keywords=${body.keywords}` : '',
      icon ? `Icon=${icon}` : '',
      '#Terminal=true # You can use this to force opening a terminal',
    ]
      .filter((x) => x)
      .join('\n');

    await fs.writeFile(filename, toWrite, 'utf-8');

    await signalProc('^mate-panel$', 'SIGUSR1');

    return toWrite;
  },
};

export default desktopEntries;
