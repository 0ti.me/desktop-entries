import * as ps from 'ps-node';

export const lookup = (opts) =>
  new Promise((resolve, reject) =>
    ps.lookup(opts, (err, results) => (err ? reject(err) : resolve(results))),
  );

export const kill = (pid, opts) =>
  new Promise((resolve, reject) =>
    ps.kill(pid, opts, (err) => (err ? reject(err) : resolve())),
  );

const signalProc = async (procName, signal) => {
  const pids = await lookup({ command: procName });

  await Promise.all(pids.map(({ pid }) => kill(pid, signal)));
};

export default signalProc;
