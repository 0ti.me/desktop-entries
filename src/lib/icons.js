import * as fs from 'fs/promises';
import * as path from 'path';

const homeLocalShareIconsDir = path.resolve(
  process.env.HOME,
  '.local/share/icons',
);
const usrShareIconsDir = '/usr/share/icons';

const pngMagic = [137, 80, 78, 71, 13, 10, 26, 10];

const tmpPrint = (x, max = 0x20) => {
  process.stderr.write('[');

  for (let i = 0; i < Math.min(max, x.length); ++i) {
    process.stderr.write(`${x[i].toString()},`);
  }

  process.stderr.write(']\n');
};

const isPng = (data) => {
  for (let i = 0; i < pngMagic.length; ++i) {
    if (data[i] !== pngMagic[i]) {
      return false;
    }
  }

  return true;
};

const isPngOrThrow = (data) => {
  for (let i = 0; i < pngMagic.length; ++i) {
    if (data[i] !== pngMagic[i]) {
      tmpPrint(pngMagic);
      tmpPrint(data);
      throw new Error(
        `png magic failed at index ${i} ${data[i]} !== ${pngMagic[i]}`,
      );
    }
  }

  return data;
};

const getSquareSizeOrThrow = (data) => {
  // width bytes 16-19
  const width = new Array(4)
    .fill(0)
    .reduce((acc, _, i) => (acc + data[i + 16]) << ((3 - i) * 8), 0);
  // height bytes 20-23
  const height = new Array(4)
    .fill(0)
    .reduce((acc, _, i) => (acc + data[i + 20]) << ((3 - i) * 8), 0);

  if (width === height) {
    return width;
  } else {
    throw new Error(`width and height did not match ${width} != ${height}`);
  }
};

const ensureDir = async (...args) => {
  const dir = path.resolve(...args);
  let stat;

  try {
    stat = await fs.stat(dir);
  } catch (err) {
    if (err.code != 'ENOENT') {
      throw err;
    }
  }

  if (!stat) {
    await fs.mkdir(dir, { recursive: true });
  } else if (stat.isFile()) {
    throw new Error('target directory exists and is a file');
  }

  return dir;
};

export default {
  write: async (filename, content) => {
    const size = getSquareSizeOrThrow(isPngOrThrow(content));
    const file = path.resolve(
      await ensureDir(homeLocalShareIconsDir, 'hicolor', `${size}x${size}`),
      filename,
    );

    await fs.writeFile(file, content);

    return file;
  },
};
