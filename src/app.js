import bodyParser from 'body-parser';
import cors from 'cors';
import express from 'express';
import * as fs from 'fs/promises';
import * as path from 'path';

import desktopEntries from './lib/desktop-entries.js';

const app = express();

app.use(cors());
app.use(bodyParser.json({ limit: '20mb' }));

app.use(express.static('web/build'));

app.use(
  '/usr/share/app-install/icons',
  express.static('/usr/share/app-install/icons'),
);

app.use(
  '/usr/share/icons/mate/256x256',
  express.static('/usr/share/icons/mate/256x256'),
);

const r = (cb) => async (req, res, next) => {
  try {
    await cb(req, res, next);
  } catch (err) {
    next(err);
  }
};

app.get(
  '/usr/share/icons/mate/256x256/apps',
  r(async (req, res) => {
    const files = await fs.readdir(req.path);

    res.status(200).send({ data: files.map((f) => path.resolve(req.path, f)) });
  }),
);

app.get(
  '/api/desktop-entries',
  r(async (_, res) => {
    const list = await desktopEntries.list();

    res.status(200).send({ data: list });
  }),
);

app.post(
  '/api/desktop-entries',
  r(async (req, res) => {
    const body = req.body ?? {};

    const toWrite = await desktopEntries.save(body);

    res.status(200).send({ data: { message: 'OK' }, meta: { body, toWrite } });
  }),
);

app.use((err, _, res, next) => {
  if (!res.headersSent) {
    res.status(500).send({ message: 'internal server error' });
  }

  console.error(err);
});

export default app;
